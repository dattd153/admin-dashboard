<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('', function () {
        return view('admin.index');
    });
    Route::resource('category', 'CategoryController', ['as' => 'admin']);
    Route::get('post/active', 'PostController@active')->name('admin.post.active');
    Route::resource('post', 'PostController', ['as' => 'admin']);
    Route::get('product/active', 'ProductController@active')->name('admin.product.active');
    Route::resource('product', 'ProductController', ['as' => 'admin']);
    Route::resource('contact', 'ContactController', ['as' => 'admin']);
    Route::get('information/', 'InformationController@index')->name('admin.information.index');
    Route::put('information/update/{id}', 'InformationController@update')->name('admin.information.update');
    Route::get('user/active', 'UserController@active')->name('admin.user.active');
    Route::resource('user', 'UserController', ['as' => 'admin']);
});
