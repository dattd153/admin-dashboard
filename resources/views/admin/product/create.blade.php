@extends('templates.admin.master') 
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Product</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add Product</h6>
        </div>
        <div class="card-body">
            <div class="col-md">
                <div class="p-5">
                  <form class="user" method="post" action="{{ route('admin.product.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label for="">Category</label>
                      <select class="form-control" name="category_id" id="">
                          @foreach ($categories as $item)
                              <option value="{{ $item->id}}">{{ $item->name}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Name</label>
                            <input type="text" name="name" class="form-control form-control-user">
                            <p class="text-danger">{{ $errors->first('name') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Price</label>
                            <input type="text" name="price" class="form-control form-control-user">
                            <p class="text-danger">{{ $errors->first('price') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Promotion price</label>
                            <input type="text" name="promotion_price" class="form-control form-control-user">
                            <p class="text-danger">{{ $errors->first('promotion_price') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Quantity</label>
                            <input type="text" name="quantity" class="form-control form-control-user">
                            <p class="text-danger">{{ $errors->first('quantity') }}</p>
                      </div>
                    </div>

                    
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Sku</label>
                            <input type="text" name="sku" class="form-control form-control-user">
                            <p class="text-danger">{{ $errors->first('sku') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Description</label>
                            <input type="text" name="description" class="form-control form-control-user">
                            <p class="text-danger">{{ $errors->first('description') }}</p>
                      </div>
                    </div>

                     <div class="form-group">
                      <label for="" class=" form-control-label">Content</label>
                      <textarea name="content" class="form-control " id="editor1">{!! old('content')!!}</textarea>
                      <p class="text-danger">{{ $errors->first('content') }}</p>
                    </div>
                    <div class="form-group">
                      <label for="">Image</label>
                      <input type="file" class="form-control-file" name="image">
                      <span class="text-danger">{{ $errors->first('image') }}</span>
                    </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
        </div>
    </div>
</div>
<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script>
  CKEDITOR.replace('editor1', options);
</script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
$('textarea.editor1').ckeditor(options);
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      if (window.File && window.FileList && window.FileReader) {
        $("#files").on("change", function(e) {
          var files = e.target.files,
            filesLength = files.length;
          for (var i = 0; i < filesLength; i++) {
            var f = files[i]
            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
              var file = e.target;
              $("<span class=\"pip\">" +
                "<img class=\"imageThumb\" width='100px' src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                "<br/><span class=\"remove\">Remove image</span>" +
                "</span>").insertAfter("#files");
              $(".remove").click(function(){
                $(this).parent(".pip").remove();
              });
              
              
            });
            fileReader.readAsDataURL(f);
          }
        });
      } else {
        alert("Your browser doesn't support to File API")
      }
    });
</script>
@endsection