@extends('templates.admin.master') 
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">post</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Update post</h6>
        </div>
        <div class="card-body">
            <div class="col-md">
                <div class="p-5">
                  <form class="user" method="post" action="{{ route('admin.post.update', $post->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Title</label>
                            <input type="text" name="title" class="form-control form-control-user"  value="{{ $post->title}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Description</label>
                            <input type="text" name="description" class="form-control form-control-user" value="{{ $post->description}}" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">content</label>
                            <input type="text" name="content" class="form-control form-control-user" value="{{ $post->content}}" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">keyword</label>
                            <input type="text" name="keyword" class="form-control form-control-user"  value="{{ $post->keyword}}">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="">Image</label>
                      <input type="file" class="form-control-file" name="image">
                      <img width="120px" class="mt-2" src="{{ asset('upload/images/'. $post->image)}}" alt="">
                    </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection