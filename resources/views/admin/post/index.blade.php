@extends('templates.admin.master') @section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Post</h1>
    <a class="btn btn-success mb-2"  href="{{ route('admin.post.create') }}">Add Post</a>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Post</h6>
            @include('errors.success')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0"
                                role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1"
                                            colspan="1" aria-sort="ascending" aria-label="Name: activate to sort
                                            column descending" style="width: 20px;">
                                            Id
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                            colspan="1" aria-label="Age:
                                            activate to sort column ascending" style="width: 51px;">
                                            Image
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                            colspan="1" aria-label="Position:
                                            activate to sort column ascending" style="width: 246px;">
                                            Title
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                            colspan="1" aria-label="Office:
                                            activate to sort column ascending" style="width: 50px;">
                                            Description
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                            colspan="1" aria-label="Age:
                                            activate to sort column ascending" style="width: 51px;">
                                            Key word
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                            colspan="1" aria-label="Age:
                                            activate to sort column ascending" style="width: 51px;">
                                            Active
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                        colspan="1" aria-label="Age:
                                        activate to sort column ascending" style="width: 90px;">
                                        Actions
                                    </th>
                                   
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($posts as $key => $item)
                                        <tr role="row" class="{{ $key %2 == 0 ? 'odd' : 'even'}}">
                                            <td class="sorting_1">{{ $item->id}}</td>
                                            <td><img style="width: 70px" src="{{ asset('upload/images/'. $item->image)}}" alt=""></td>
                                            <td>{{ $item->title}}</td>
                                            <td>{{ $item->description}}</td>
                                            <td>{{ $item->keyword}}</td>
                                            <td id="active-{{ $item->id }}"> 
                                                @if ($item->active == 1)
                                                  <a href="javascript:void(0)" onclick="getActive({{$item->id}})" style="cursor: pointer"><i class="fas fa-check text-success"></i> </a>
                                                @else
                                                  <a href="javascript:void(0)" onclick="getActive({{$item->id}})" style="cursor: pointer"><i class="fas fa-times text-danger"></i></a>
                                                @endif
                                              </td>
                                            <td>
                                                <form action="{{ route('admin.post.destroy', $item->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a class="btn btn-danger" href="" data-toggle="modal" data-target="#modelId-{{ $item->id}}">Delete</a>
                                                    <a class="btn btn-primary" href="{{ route('admin.post.edit', $item->id)}}" role="button">Edit</a>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="modelId-{{ $item->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Delete post</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Are you sure?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function getActive(id){
       $.ajax({
         url: "{{route('admin.post.active')}}",
         type: 'GET',
         cache: false,
         data: {
               id: id,
           },
         success: function(data){
           console.log('success')
           $('#active-'+id).html(data);
         },
         error: function() {
          alert("Có lỗi");
        }
      });
       return false;
     }
</script>
@endsection
