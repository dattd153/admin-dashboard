@extends('templates.admin.master') 
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">User</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit User</h6>
        </div>
        <div class="card-body">
            <div class="col-md">
                <div class="p-5">
                  <form class="user" method="post" action="{{ route('admin.user.update', $user->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <div class="col-sm-12 mb-3 ">
                          <label for="">Name</label>
                            <input type="text" name="name" class="form-control form-control-user" value="{{ $user->name}}">
                            <p class="text-danger">{{ $errors->first('name') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 ">
                            <label for="">Email</label>
                                <input type="text" name="email" class="form-control form-control-user" value="{{ $user->email}}">
                                <p class="text-danger">{{ $errors->first('email') }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Role</label>
                        <select class="custom-select" name="role">
                            <option value="">-----</option>
                            <option {{$user->role == 0 ? 'selected' : ''}} value="0">User</option>
                            <option {{$user->role == 1 ? 'selected' : ''}} value="1">Admin</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('role') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Active</label>
                        <select class="custom-select" name="active">
                            <option value="">-----</option>
                            <option {{$user->active == 0 ? 'selected' : ''}} value="0">None</option>
                            <option {{$user->active == 1 ? 'selected' : ''}} value="1">Active</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('active') }}</p>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection