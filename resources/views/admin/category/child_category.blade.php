<li class="mb-3">
    <form class="" action="{{ route('admin.category.destroy', $child_category->id)}}" method="post">
        <div class="sub-category">
            <span class="mr-2">{{ $child_category->name }}</span>
            <img style="width: 100px; object-fit: cover" class="" src="{{ asset('upload/images/'. $child_category->image)}}" alt="">
            @csrf
            @method('DELETE')
            <a class="btn btn-danger" href="" data-toggle="modal" data-target="#modelId-{{ $child_category->id}}">Delete</a>
            <a class="btn btn-primary" href="{{ route('admin.category.edit', $child_category->id)}}" role="button">Edit</a>
            <!-- Modal -->
            <div class="modal fade" id="modelId-{{ $child_category->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete category</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">
                            Are you sure?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</li>
@if ($child_category->children)
    <ul>
        @foreach ($child_category->children as $childCategory)
            @include('admin.category.child_category', ['child_category' => $childCategory])
        @endforeach
    </ul>
@endif
