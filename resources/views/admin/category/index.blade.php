@extends('templates.admin.master') @section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Category</h1>
    <a class="btn btn-success mb-2"  href="{{ route('admin.category.create') }}">Add category</a>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Category</h6>
            @include('errors.success')
        </div>
        <div class="card-body">
                <ul>
                    @foreach ($categories as $category)
                        <li class="mb-3">
                            <form class="" action="{{ route('admin.category.destroy', $category->id)}}" method="post">
                                <div class="sub-category">
                                    <span class="mr-2">{{ $category->name }}</span>
                                    <img style="width: 100px; object-fit: cover" class="" src="{{ asset('upload/images/'. $category->image)}}" alt="">
                                    @csrf
                                    @method('DELETE')
                                    <a class="btn btn-danger" href="" data-toggle="modal" data-target="#modelId-{{ $category->id}}">Delete</a>
                                    <a class="btn btn-primary" href="{{ route('admin.category.edit', $category->id)}}" role="button">Edit</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="modelId-{{ $category->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete category</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
                       
                        <ul>
                            @foreach ($category->children as $childCategory)
                                @include('admin.category.child_category', ['child_category' => $childCategory])
                            @endforeach
                        </ul>
                    @endforeach
            	</ul>
        </div>
    </div>
</div>
@endsection