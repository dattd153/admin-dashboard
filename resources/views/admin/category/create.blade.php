@extends('templates.admin.master') 
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Category</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add Category</h6>
        </div>
        <div class="card-body">
            <div class="col-md">
                <div class="p-5">
                  <form class="user" method="post" action="{{ route('admin.category.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Name</label>
                            <input type="text" name="name" class="form-control form-control-user" id="exampleFirstName" placeholder="First Name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                            <label for="">Parent</label>
                            <select class="custom-select" name="parent_id" id="">
                                <option selected value="{{ null}}">Prarent category</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id}}">{{ $item->name}}</option>
                                @endforeach
                            </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="">Image</label>
                      <input type="file" class="form-control-file" name="image">
                    </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection