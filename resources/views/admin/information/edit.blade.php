@extends('templates.admin.master') 
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Infomation</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit infomation</h6>
        </div>
        <div class="card-body">
            <div class="col-md">
                <div class="p-5">
                  @include('errors.success')
                  <form class="user" method="post" action="{{ route('admin.information.update', $information->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                      <label for="">Logo</label>
                      <input type="file" class="form-control-file" name="logo">
                      <span class="text-danger">{{ $errors->first('logo') }}</span>
                      <img class="mt-3" width="150px" src="{{ asset('upload/images/' . $information->logo)}}" alt="">
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Slogan</label>
                            <input type="text" name="slogan" class="form-control form-control-user" value="{{ $information->slogan}}">
                            <p class="text-danger">{{ $errors->first('slogan') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Phone</label>
                            <input type="text" name="phone" class="form-control form-control-user" value="{{ $information->phone}}">
                            <p class="text-danger">{{ $errors->first('phone') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Address</label>
                            <input type="text" name="address" class="form-control form-control-user" value="{{ $information->address}}">
                            <p class="text-danger">{{ $errors->first('address') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Email</label>
                            <input type="text" name="email" class="form-control form-control-user" value="{{ $information->email}}">
                            <p class="text-danger">{{ $errors->first('email') }}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Copy right</label>
                            <input type="text" name="coppyright" class="form-control form-control-user" value="{{ $information->coppyright}}">
                            <p class="text-danger">{{ $errors->first('coppyright') }}</p>
                      </div>
                    </div>
                    
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label for="">Link banner</label>
                            <input type="text" name="banner_link" class="form-control form-control-user" value="{{ $information->banner_link}}">
                            <p class="text-danger">{{ $errors->first('banner_link') }}</p>
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="">Banner image</label>
                        <input type="file" class="form-control-file" name="banner_image">
                        <span class="text-danger">{{ $errors->first('banner_image') }}</span>
                        <img class="mt-3" width="150px" src="{{ asset('upload/images/' . $information->banner_image)}}" alt="">
                      </div>

                      <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection