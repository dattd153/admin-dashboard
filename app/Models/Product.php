<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'category_id',
        'name',
        'slug',
        'image',
        'price',
        'promotion_price',
        'quantity',
        'sku',
        'description',
        'content',
        'active',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
