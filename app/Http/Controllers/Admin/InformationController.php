<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Information;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class InformationController extends Controller
{
    public function index()
    {
        $information = Information::get()->first();
        return view('admin.information.edit', compact('information'));
    }

    public function update(Request $request, $id)
    {
        $information = Information::get()->first();
        $validator = Validator::make($request->all(), [
            'slogan' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'email' => 'required',
            'banner_link' => 'required',
            'coppyright' => 'required',
            'logo' => 'mimes:jpeg,jpg,png,gif|max:5000' // max 5000kb
        ], [
            'slogan.required' => 'Bạn cần nhập slogan',
            'phone.required' => 'Bạn cần nhập số điện thoại',
            'address.required' => 'Bạn cần nhập địa chỉ',
            'email.required' => 'Bạn cần nhập email',
            'banner_link.required' => 'Bạn cần nhập link banner',
            'coppyright.required' => 'Bạn cần nhập coppyright',
            'logo.mimes' => 'Bạn cần tải hình đúng với định dạng jpeg, jpg, png, gif',
            'logo.max' => 'Kích thược hình ảnh không vượt quá 5M',
            'banner_image.mimes' => 'Bạn cần tải hình đúng với định dạng jpeg, jpg, png, gif',
            'banner_image.max' => 'Kích thược hình ảnh không vượt quá 5M',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $information->slogan = $request->slogan;
        $information->phone = $request->phone;
        $information->address = $request->address;
        $information->email = $request->email;
        $information->coppyright = $request->coppyright;
        $information->banner_link = $request->banner_link;
        $information->updated_at = Carbon::now('Asia/Ho_Chi_Minh');
        if ($request->file('logo') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $information->logo;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            $file = $request->file('logo');
            $filename = time() . $file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $information->logo = $filename;
        } 
        if ($request->file('banner_image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $information->banner_image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            $file = $request->file('banner_image');
            $filename = time() . $file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $information->banner_image = $filename;
        } 
        $information->save();
        return back()->with('success', 'Cập nhật thông tin thành công!');
    }
}
