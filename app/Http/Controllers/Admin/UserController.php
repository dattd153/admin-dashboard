<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(10);
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'email' => 'required|email',
            'password' => 'required',
            're_password' => 'same:password|min:6',
            'role' => 'required',
            'active' => 'required',
        ],[
            'name.required' => 'Bạn cần phải nhập tên người dùng',
            'email.required' => 'Bạn cần phải nhập email',
            'password.required' => 'Bạn cần phải nhập mật khẩu',
            're_password.same' => 'Mật khẩu xác nhận cần giống với mật khẩu',
            'role.required' => 'Bạn cần phải chọn role',
            'active.required' => 'Bạn cần phải chọn active',
        ]);
            
        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } 
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        $user->active = $request->active;

        $user->save();
        return redirect()->route('admin.user.index')->with('success', 'Thêm người dùng thành công');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'email' => 'required|email',
            'role' => 'required',
            'active' => 'required',
        ],[
            'name.required' => 'Bạn cần phải nhập tên người dùng',
            'email.required' => 'Bạn cần phải nhập email',
            'role.required' => 'Bạn cần phải chọn role',
            'active.required' => 'Bạn cần phải chọn active',
        ]);
            
        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } 
        }
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->active = $request->active;

        $user->save();
        return redirect()->route('admin.user.index')->with('success', 'Cập nhật người dùng thành công');
    }
    public function destroy(Request $request,$id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('admin.user.index')->with('success', 'Xóa người dùng thành công');
    }

    public function active(Request $request)
    {
        $id = $request->id;
        $user = User::findOrFail($id);
        if ($user->active == 0) {
            $user->active = 1;
            $user->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fas fa-check text-success"></i> </a>';
        } else {
            $user->active = 0;
            $user->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fas fa-times text-danger"></i> </a>';
        }
    }
}
