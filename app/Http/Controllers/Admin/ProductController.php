<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::OrderBy('id', 'desc')->paginate(50);
        return view('admin.product.index', compact('products'));
    }
    
    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', compact('categories'));
    }
   
    public function store(Request $request)
    {   
        $product = new Product();
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:products',
            'sku' => 'required',
            'description' => 'required',
            'content'=>'required',
            'quantity'=>'required',
            'price' => 'required',
            'promotion_price' => 'required',
            'image' => 'required',
        ],[
            'name.required' => 'Bạn cần nhập tên sản phẩm',
            'sku.required' => 'Bạn cần nhập mã sản phẩm',
            'description.required' => 'Bạn cần nhập mô tả',
            'content.required' => 'Bạn cần nhập nội dung của sản phẩm',
            'price.required' => 'Bạn cần nhập giá của sản phẩm',
            'quantity.required' => 'Bạn cần nhập só lượng của sản phẩm',
            'promotion_price.required' => 'Bạn cần nhập giá khuyến mãi của sản phẩm',
            'image.required' => 'Bạn cần tải ảnh lên',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 

        $product->category_id  = $request->category_id; 
        $product->name  = $request->name; 
        $product->sku  = $request->sku; 
        $product->slug  = Str::slug($request->name); 
        $product->description  = $request->description; 
        $product->content =$request->content;
        $product->price  = $request->price; 
        $product->promotion_price  = $request->promotion_price; 
        $product->created_at = Carbon::now('Asia/Ho_Chi_Minh');
        $product->updated_at = Carbon::now('Asia/Ho_Chi_Minh');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $product->image = $filename;
        } 
        $product->save();
        return redirect()->route('admin.product.index')->with('success', 'Thêm sản phẩm thành công!');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::all();
        return view('admin.product.edit', compact('product', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'unique:products,name,'. $id,
        ],[
            'name.unique' => 'Tên sản phẩm đã tồn tại'
        ]);
            
        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } 
        }

        $product->category_id  = $request->category_id; 
        $product->name  = $request->name; 
        $product->sku  = $request->sku; 
        $product->slug  = Str::slug($request->name); 
        $product->description  = $request->description; 
        $product->content =$request->content;
        $product->price  = $request->price; 
        $product->promotion_price  = $request->promotion_price; 
        $product->created_at = Carbon::now('Asia/Ho_Chi_Minh');
        $product->updated_at = Carbon::now('Asia/Ho_Chi_Minh');
        
        if ($request->file('image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $product->image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $product->image = $filename;
        } 
        
        $product->save();
        return redirect()->route('admin.product.index')->with('success', 'Cập nhật sản phẩm thành công!');
    }
    
    public function destroy(Request $request,$id)
    {
        $product = Product::findOrFail($id);
        $path = public_path('upload/images/');
        $file_old = $path . $product->image;
        if(File::exists($file_old)) {
            File::delete($file_old); 
        }
        $product->delete();
        return redirect()->route('admin.product.index')->with('success', 'Xóa sản phẩm thành công');
    }

    public function active(Request $request)
    {
        $id = $request->id;
        $product = Product::findOrFail($id);
        if ($product->active == 0) {
            $product->active = 1;
            $product->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fas fa-check text-success"></i> </a>';
        } else {
            $product->active = 0;
            $product->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fas fa-times text-danger"></i> </a>';
        }
    }
}
