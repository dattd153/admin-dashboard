<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::whereNull('parent_id')
                                ->with('children')
                                ->get();
        return view('admin.category.index', compact('categories'));

    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.category.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->parent_id = $request->parent_id;

        if ($request->file('image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $category->image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $category->image = $filename;
        } 
        $category->save();

        return redirect()->route('admin.category.index')->with('success', 'Create category successfully');
    }

    public function show($id)
    {
    }
    
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $categories = Category::all();
        return view('admin.category.edit', compact('category', 'categories'));
      
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->parent_id = $request->parent_id;

        if ($request->file('image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $category->image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $category->image = $filename;
        } 
        $category->save();

        return redirect()->route('admin.category.index')->with('success', 'Update category successfully');
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $path = public_path('upload/images/');
        $file_old = $path . $category->image;
        if(File::exists($file_old)) {
            File::delete($file_old); 
        }
        $category->delete();
        return redirect()->route('admin.category.index')->with('success', 'Delete category successfully');
    }
}
