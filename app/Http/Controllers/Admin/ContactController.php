<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $contacts = Contact::OrderBy('id', 'desc')->paginate(50);
        return view('admin.contact.index', compact('contacts'));
    }
 
    public function destroy(Request $request,$id)
    {
        $contact = contact::findOrFail($id);
        $contact->delete();
        return redirect()->route('admin.contact.index')->with('success', 'Xóa liên hệ thành công');
    }
}