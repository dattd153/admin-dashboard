<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('admin.post.index', compact('posts'));
    }

    public function create()
    {
        return view('admin.post.create');
    }

    public function store(Request $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->slug = Str::slug($request->name);
        $post->description = $request->description;
        $post->content = $request->content;
        $post->keyword = $request->keyword;
        $post->active = 1;

        if ($request->file('image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $post->image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $post->image = $filename;
        } 
        $post->save();

        return redirect()->route('admin.post.index')->with('success', 'Create post successfully');
    }

    public function show($id)
    {
    }
    
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.post.edit', compact('post'));
      
    }

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->title = $request->title;
        $post->slug = Str::slug($request->name);
        $post->description = $request->description;
        $post->content = $request->content;
        $post->keyword = $request->keyword;

        if ($request->file('image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $post->image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $post->image = $filename;
        } 
        $post->save();

        return redirect()->route('admin.post.index')->with('success', 'Update post successfully');
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $path = public_path('upload/images/');
        $file_old = $path . $post->image;
        if(File::exists($file_old)) {
            File::delete($file_old); 
        }
        $post->delete();
        return redirect()->route('admin.post.index')->with('success', 'Delete post successfully');
    }

    public function active(Request $request)
    {
        $id = $request->id;
        $product = Post::findOrFail($id);
        if ($product->active == 0) {
            $product->active = 1;
            $product->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fas fa-check text-success"></i> </a>';
        } else {
            $product->active = 0;
            $product->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fas fa-times text-danger"></i> </a>';
        }
    }
}